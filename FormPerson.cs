﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kontr3
{
    public partial class FormPerson : Form
    {
        public FormPerson()
        {
            InitializeComponent();
            name = " ";
            lastname = " ";
            birthday = System.DateTime.MinValue;
        }

        private void FormPerson_Load(object sender, EventArgs e)
        {
            textBox1.Text = name;
        }

        private void FormPerson_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            base.Hide();
        }

        public String name { get; set; }
        public String lastname{get;set;}
        public DateTime birthday{get ; set ; }

        private void FormPerson_VisibleChanged(object sender, EventArgs e)
        {
            textBox1.Text = name;
            textBox2.Text = lastname;
            textBox3.Text = birthday.ToString("MM/dd/yyyy");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
